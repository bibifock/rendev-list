--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
ALTER TABLE files ADD isA3 INTEGER NULL COLLATE NOCASE;
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
BEGIN TRANSACTION;

ALTER TABLE files RENAME TO _files_old;

CREATE TABLE files (
    name TEXT NOT NULL COLLATE NOCASE,
    type TEXT NOT NULL COLLATE NOCASE,
    fullpath TEXT NOT NULL,
    songId INTEGER NOT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    display INTEGER NULL COLLATE NOCASE,
    label TEXT NULL COLLATE NOCASE,
    PRIMARY KEY (fullpath, songId)
)

INSERT INTO files (name, type, fullpath, songId, created, updated, display, label)
  SELECT name, type, fullpath, songId, created, updated, display, label
  FROM _files_old;

DROP TABLE _files_old;

COMMIT;
