--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE tabs (
    title TEXT NOT NULL COLLATE NOCASE,
    artist TEXT NULL COLLATE NOCASE,
    author TEXT NULL COLLATE NOCASE,
    link TEXT NULL COLLATE NOCASE,
    categoryId INTEGER NOT NULL,
    level INTEGER DEFAULT 1,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (title, artist)
);

CREATE TABLE categories (
    name TEXT NOT NULL PRIMARY KEY COLLATE NOCASE
);

INSERT INTO categories (name)
VALUES
    ('Chanson Internationale'),
    ('Chanson Française');

CREATE TABLE files (
    name TEXT NOT NULL COLLATE NOCASE,
    type TEXT NOT NULL COLLATE NOCASE,
    fullpath TEXT NOT NULL,
    idTab INTEGER NOT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (fullpath, idTab)
);

CREATE TABLE users (
    email TEXT NOT NULL PRIMARY KEY COLLATE NOCASE,
    name TEXT NULL COLLATE NOCASE,
    password TEXT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    lastConnection TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
DROP TABLE tabs;

DROP TABLE categories;

DROP TABLE files;

DROP TABLE users;
