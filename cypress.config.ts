import { defineConfig } from 'cypress';

import { addNewUser } from './cypress/plugins/database';

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:1974/',
    //baseUrl: 'http://localhost:5173/',
    video: false,
    setupNodeEvents(on) {
      on('task', {
        'db:addNewUser': addNewUser
      });
    }
  }
});
