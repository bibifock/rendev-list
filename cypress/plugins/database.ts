import SQL from 'sql-template-strings';
import { open } from 'sqlite';
import sqlite3 from 'sqlite3';

const getDb = () =>
  open({
    filename: process.env.VITE_PUBLIC_DB_SOURCE ?? '',
    driver: sqlite3.Database
  });

const makeAddSuffix =
  (suffix = Date.now()) =>
  (str: string) =>
    `${str}__${suffix}`;

export const addNewUser = async () => {
  if (!process.env.CYPRESS_ALLOW_MUTATIONS) {
    throw new Error("[ERROR] mutations aren't allowed");
  }
  const db = await getDb();

  const addSuffix = makeAddSuffix();

  const user = {
    name: addSuffix('user-test'),
    email: addSuffix('user-test') + '@mail.com',
    password: '01234567890'
  };

  const query = SQL`
    INSERT INTO users (email, name) VALUES
    (${user.email}, ${user.name})
  `;

  await db.run(query);

  return user;
};
