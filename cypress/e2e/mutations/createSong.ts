export const createSong = (newSong) => {
  cy.get('input[name="title"]').type(newSong.title);
  cy.get('input[name="artist"]').type(newSong.artist);
  cy.get('input[name="author"').type(newSong.author);
  cy.get('input[name="youtube-link"]').type(newSong.link);
  cy.get('input[name="level"').type(newSong.level);

  cy.get('input[type=file]').last().invoke('show').selectFile(newSong.file);
  cy.get('input[type=file]').last().invoke('hide');
  cy.contains('span', newSong.file.replace(/^.*\//, ''));

  cy.get('.rating').first().click();

  cy.getCyId('save-song-action').click();
};
