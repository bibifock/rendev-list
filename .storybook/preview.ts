import './bootstrap.min.css';
import './global.css';

import BodyDecorator from './BodyDecorator.svelte';

import type { Preview } from '@storybook/svelte';

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/
      }
    }
  },
  decorators: [() => ({ Component: BodyDecorator })]
};

export default preview;
