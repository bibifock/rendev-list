import { error } from '@sveltejs/kit';

import { loadCategories } from '$lib/data-loaders/loadCategories';
import { loadSong } from '$lib/data-loaders/loadSong';
import type { Song } from '$lib/types';
import type { Option } from '$lib/ui/types';

import type { PageServerLoad } from './$types';

export const load = (async ({ params, parent }) => {
  const { session } = await parent();
  const logged = !!session?.id;
  const id = parseInt(params.id);
  if (typeof id !== 'number' || id < 0) {
    error(404, 'Bad song url');
  }

  const categories = await loadCategories();

  if (id === 0) {
    if (!logged) {
      error(403, 'please go see here');
    }
    return {
      categories
    };
  }

  const song = await loadSong({ id, logged });

  return {
    song,
    categories
  };
}) satisfies PageServerLoad<{ song?: Song; categories: Option[] }>;
