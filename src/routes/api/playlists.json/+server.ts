import { json } from '@sveltejs/kit';

import loadPlaylists from '$lib/data-loaders/loadPlaylists';

import type { RequestHandler } from '@sveltejs/kit';

export const POST = (async ({ request }) => {
  const { search, sort, page, limit, type } = await request.json();

  const result = await loadPlaylists({
    search,
    sort,
    page,
    limit,
    type
  });

  return json(result);
}) satisfies RequestHandler;
