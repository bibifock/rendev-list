import { json, error } from '@sveltejs/kit';

import { haveError } from '$lib/controllers/playlist/haveError';
import { preparePlaylist } from '$lib/data-loaders/preparePlaylist';
import playlistModel from '$lib/models/playlist/Playlist';
import { URL_EDIT } from '$lib/views/playlist/constants';

import type { RequestHandler } from '@sveltejs/kit';

export const POST = (async ({ request, params, locals }) => {
  const logged = !!locals.session.data?.id;
  if (!logged) {
    error(403, 'Sorry, this is not possible');
  }

  const playlist = await request.json();
  const id = parseInt(params.id);

  const errors = haveError(playlist, {}) as Record<string, string>;

  const result = await playlistModel.getByName(playlist.name);
  if (result && id !== result.id) {
    const { id: rowid, name } = result;
    errors.name = `${name} already present <a href="${URL_EDIT.replace(
      '#ID#',
      `${rowid}`
    )}" class="alert-link" target="_blank">here</a>`;
  }

  if (Object.keys(errors).length) {
    error(400, { message: Object.keys(errors).join(', ') });
  }

  const { id: idPlaylist } = await playlistModel.save({ id, ...playlist });

  const finalPlaylist = await playlistModel.getById(idPlaylist, { edit: true });

  if (!finalPlaylist) {
    error(404, 'not found');
  }

  return json(preparePlaylist(finalPlaylist));
}) satisfies RequestHandler<{ id: string }>;
