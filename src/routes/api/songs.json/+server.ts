import { json } from '@sveltejs/kit';
import sortBy from 'lodash/sortBy';

import songModel from '$lib/models/song/Song';
import getOrderBySort from '$lib/models/song/getOrderBySort';
import prepareSong from '$lib/views/song/list/prepareSong';
import type { SongSort, Song } from '$lib/types';

import type { RequestHandler } from '@sveltejs/kit';

export const POST = (async ({ request }) => {
  const { sort, byIndex, ...params } = await request.json();

  const { items: rawItems, ...rest } = await songModel.all({
    ...params,
    orderBy: getOrderBySort(sort ?? 'titleAsc')
  });

  const songs: ReturnType<typeof prepareSong>[] = rawItems.map(prepareSong);
  if (!byIndex) {
    return json({ ...rest, items: songs });
  }
  const sectionKey = sort.startsWith('title')
    ? 'sectionTitle'
    : sort.startsWith('artist')
      ? 'sectionArtist'
      : undefined;

  const songsByIndex = Object.values(
    songs.reduce<Record<string, { section: string; items: Song[] }>>(
      (acc, song) => {
        const section: string =
          sectionKey && song?.[sectionKey] ? song[sectionKey] : '';
        return {
          ...acc,
          [section]: {
            section,
            items: [...(acc?.[section]?.items ?? []), song]
          }
        };
      },
      {}
    )
  );
  const items = sortBy(songsByIndex, 'section');
  if (sort === 'titleDesc' || sort === 'artistDesc') {
    items.reverse();
  }

  return json({ ...rest, items });
}) satisfies RequestHandler<{ sort: SongSort }>;
