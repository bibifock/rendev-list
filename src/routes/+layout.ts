import { loadLocale } from '$lib/i18n';

import type { LayoutLoad } from './$types';

export const load = (async () => {
  await loadLocale();
}) satisfies LayoutLoad;
