import { loadCategories } from '$lib/data-loaders/loadCategories';
import { preparePlaylist } from '$lib/data-loaders/preparePlaylist';
import playlistModel from '$lib/models/playlist/Playlist';

import type { PageServerLoad } from './$types';

export const load = (async () => {
  const [lastPlaylist, categories] = await Promise.all([
    playlistModel.getLast(),
    loadCategories()
  ]);

  if (!lastPlaylist) {
    return {
      categories
    };
  }

  const playlist = preparePlaylist(lastPlaylist);

  return {
    categories,
    playlist
  };
}) satisfies PageServerLoad;
