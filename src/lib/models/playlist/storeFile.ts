import fs from 'fs';
import nPath from 'path';

import { UPLOAD_PATH_LIST, UPLOAD_DIR } from '$lib/models/file/constants';

const getRealPath = (path: string) => process.env.PWD + UPLOAD_DIR + path;

export type StorableFile = {
  path: string;
  name: string;
  type: string;
};

type StoreFileArgs = {
  src?: string;
  name: string;
  file?: StorableFile;
};

const storeFile = ({ src, file, name }: StoreFileArgs) => {
  if (!file) {
    return src;
  }

  const { path } = file;
  const ext = nPath.extname(path);
  const newFile = `${UPLOAD_PATH_LIST}/${name}${ext}`;

  if (src && fs.existsSync(getRealPath(src))) {
    fs.unlinkSync(getRealPath(src));
  }

  fs.copyFileSync(path, getRealPath(newFile));

  return newFile;
};

export default storeFile;
