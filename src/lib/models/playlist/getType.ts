import type { PlaylistType } from '$lib/types';

import { PLAYLIST_TYPE_SONGBOOK, PLAYLIST_TYPE_ALMANAC } from '../constants';


export const getType = (type: number): PlaylistType => {
  switch (type) {
    case PLAYLIST_TYPE_ALMANAC:
      return 'almanac';

    case PLAYLIST_TYPE_SONGBOOK:
      return 'songbook';

    default:
      return 'playlist';
  }
};
