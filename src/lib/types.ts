const playlistTypes = ['almanac', 'playlist', 'songbook'] as const;
export type PlaylistType = (typeof playlistTypes)[number];

export const isPlaylistType = (type: unknown): type is PlaylistType =>
  typeof type === 'string' && playlistTypes.includes(type as PlaylistType);

const playlistSorts = ['nameAsc', 'nameDesc'] as const;
export type PlaylistSort = (typeof playlistSorts)[number];
export const isPlaylistSort = (type: unknown): type is PlaylistSort =>
  typeof type === 'string' && playlistSorts.includes(type as PlaylistSort);

const songSorts = [
  'titleAsc',
  'titleDesc',
  'artistAsc',
  'artistDesc',
  'createdDesc',
  'updatedDesc'
] as const;
export type SongSort = (typeof songSorts)[number];
export const isSongSort = (type: unknown): type is SongSort =>
  typeof type === 'string' && songSorts.includes(type as SongSort);

export type UploadedFile = {
  name: string;
  path: string;
  type: string;
};

export type Image = PlaylistImage | { file: UploadedFile } | undefined;

export type SongFile = {
  id?: number;
  name: string;
  type: string;
  selected?: boolean;
  fullpath?: string;
};

export type SongData = {
  position?: number;
  title: string;
  artist: string;
  author?: string;
  link?: string;
  level?: number;
  fileId?: number;
  fullpath?: string;
  categoryId?: number;
  isA3?: number;
  id?: number;
  created?: string | Date;
  updated?: string | Date;
  files?: SongFile[];
  file?: SongFile;
};

export type Song = SongData & {
  category: string;
  href: string;
  lastPlaylistDate?: string;
};

export type PlaylistModel = {
  name: string;
  image: string;
  playlist: string;
  file: string | null;
  created: string;
  updated: string;
  type: PlaylistType;
  id: number;
  songs: Song[];
};

export type PlaylistImage = {
  src: string;
  alt: string;
};

export type PlaylistFile = {
  name: string;
  fullpath?: string;
};

export type Playlist = Omit<
  PlaylistModel,
  'file' | 'image' | 'updated' | 'created' | 'type'
> & {
  id?: number;
  file?: PlaylistFile;
  image: PlaylistImage;
  type: PlaylistType;
  href: string;
  updated: Date;
  created: Date;
  songs: Song[];
  _search: string;
};

export type PlaylistSong = Omit<PlaylistModel, 'name' | 'playlist' | 'file'> & {
  title: string;
  youtube: string;
  href: string;
  file?: string;
};

export type Playlists = {
  page: number;
  total: number;
  playlists: Playlist[];
};

// generics
export type Paginate<T> = { items: T[]; total: number; page: number };
