import post from '$lib/utils/post';
import type { PlaylistModel } from '$lib/types';

import { API_URL_WISHES, URL_EDIT as URL_EDIT_SONG } from '../constants';
import { preparePlaylist } from '../list/preparePlaylist';


type WishResponse = {
  playlists: (PlaylistModel & { position: number })[];
  id: string;
};

const parseWish = ({ playlists, ...rest }: WishResponse) => ({
  ...rest,
  href:
    playlists.findIndex((v) => v.position) > -1
      ? URL_EDIT_SONG.replace('#ID#', rest.id)
      : undefined,
  playlists: playlists.map(({ songs, ...restItem }) =>
    preparePlaylist({ ...restItem, items: songs })
  )
});

export const searchWishes = (params: Record<string, string>) =>
  post<WishResponse[]>(API_URL_WISHES, params).then((items) =>
    items.map(parseWish)
  );
