import type { Song } from '$lib/types';

import prepareSong from './prepareSong';


export function preparePlaylist<
  T extends { items: Song[] } = { items: Song[] }
>({ items, ...rest }: T) {
  return {
    ...rest,
    items: items.map(prepareSong)
  };
}
