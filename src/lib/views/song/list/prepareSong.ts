import { parseDate } from '$lib/utils/date';
import { URL_EDIT as URL_EDIT_PLAYLIST } from '$lib/views/playlist/constants';
import type { Song, SongFile, PlaylistSong, PlaylistModel } from '$lib/types';

import { URL_EDIT } from '../constants';

const prepareFiles = (files?: SongFile[], fileId?: number): SongFile[] => {
  if (!files || !files.length) {
    return [];
  }

  if (!fileId) {
    return files;
  }
  const index = files.findIndex(({ id }) => id === fileId);
  if (index > -1) {
    files[index].selected = true;
  }

  return files;
};

export const prepareSongPlaylists = (
  playlists?: PlaylistModel[]
): PlaylistSong[] => {
  if (!playlists || !playlists.length) {
    return [];
  }

  return playlists.map(({ file, name: title, playlist: youtube, ...item }) => ({
    ...item,
    file: file ?? undefined,
    href: URL_EDIT_PLAYLIST.replace('#ID#', `${item.id}`),
    title,
    youtube
  }));
};

const getSongSection = (title: string) =>
  title
    .toLowerCase()
    // remove all accents
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    // then only valid index chars
    .replace(/^[^0-9a-z]+/, '')[0]
    // then replace number
    .replace(/[0-9]/, '#');

type PrepareSongArgs = Song & {
  playlists?: PlaylistModel[];
  files?: SongFile[];
};

const prepareSong = ({
  created,
  updated,
  fileId,
  file: selectedFile,
  files: rawFiles,
  id,
  playlists,
  ...rest
}: PrepareSongArgs): Song & {
  playlists: PlaylistSong[];
  sectionTitle: string;
  sectionArtist: string;
} => {
  const files = prepareFiles(rawFiles, fileId);
  const file = selectedFile ? selectedFile : files.find((f) => f.selected);
  return {
    ...rest,
    sectionArtist: getSongSection(rest.artist),
    sectionTitle: getSongSection(rest.title),
    id,
    fileId,
    href: URL_EDIT.replace('#ID#', `${id}`),
    created: parseDate(created),
    updated: parseDate(updated),
    files: prepareFiles(files, fileId),
    file,
    playlists: prepareSongPlaylists(playlists)
  };
};

export default prepareSong;
