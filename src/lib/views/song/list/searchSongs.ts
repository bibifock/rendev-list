import post from '$lib/utils/post';
import type { Paginate, Song } from '$lib/types';

import { API_URL_INDEX } from '../constants';

import { preparePlaylist } from './preparePlaylist';


type Params = Partial<{ search: string; limit: number }>;

export async function searchSongs(params: Params): Promise<Paginate<Song>> {
  const result = await post<Paginate<Song>, Params>(API_URL_INDEX, params);

  return preparePlaylist(result);
}
