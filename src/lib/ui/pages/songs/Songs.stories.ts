import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';

import Songs from './Songs.svelte';
import { total } from './songs-list.json';


const meta: CustomMeta<typeof Songs> = {
  title: 'pages/Songs',
  component: Songs,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
  tags: ['autodocs'],
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/svelte/configure/story-layout
    layout: 'padded'
  },
  args: {
    total,
    filters: {
      categories: [],
      levels: []
    }
  }
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

export const Base: Story = {};
