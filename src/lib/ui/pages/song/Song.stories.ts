import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';

import Component from './Song.svelte';


const meta: CustomMeta<typeof Component> = {
  title: 'pages/Song',
  component: Component,
  tags: ['autodocs'],
  args: {
    categories: [],
    loadArtists: () => Promise.resolve([]),
    loadAuthors: () => Promise.resolve([])
  }
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

export const Base: Story = {};

export const Edition: Story = {
  args: {
    edit: true,
    defaultAuthor: 'bibi'
  }
};
