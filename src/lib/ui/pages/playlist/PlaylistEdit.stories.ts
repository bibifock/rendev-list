import { playlistFactory } from '$lib/utils/testing/factory/Playlist';
import songFactory from '$lib/utils/testing/factory/Song';
import faker from '$lib/utils/testing/faker';
import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';

import Playlist from './Playlist.svelte';


const meta: CustomMeta<typeof Playlist> = {
  title: 'Pages/Playlist/Edit',
  component: Playlist,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
  tags: ['autodocs'],
  args: {
    categories: [],
    type: 'playlist',
    canEdit: true,
    loadArtists: () => Promise.resolve([]),
    loadSongs: () => Promise.resolve([]),
    loadAuthors: () => Promise.resolve([])
  },
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/svelte/configure/story-layout
    layout: 'fullscreen'
  }
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

export const Empty: Story = {};

export const Filled: Story = {
  args: {
    playlist: playlistFactory({
      file: {
        name: faker.date.past().toISOString().replace(/T.+/, ''),
        fullpath: faker.internet.url()
      },
      image: {
        src: 'https://www.fillmurray.com/200/300'
      },
      songs: Array.from(Array(12)).map((_, i) =>
        songFactory({ id: i + 1, position: i < 6 ? i + 1 : undefined })
      )
    })
  }
};
