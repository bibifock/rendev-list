import { playlistFactory } from '$lib/utils/testing/factory/Playlist';
import songFactory from '$lib/utils/testing/factory/Song';
import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';

import Playlist from './Playlist.svelte';


const meta: CustomMeta<typeof Playlist> = {
  title: 'Pages/Playlist/Consult',
  component: Playlist,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
  tags: ['autodocs'],
  args: {
    playlist: {
      type: 'playlist',
      songs: []
    }
  },
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/svelte/configure/story-layout
    layout: 'fullscreen'
  }
};

export default meta;
type Story = CustomStoryObj<typeof meta>;

export const Filled: Story = {
  args: {
    playlist: playlistFactory({
      songs: Array.from(Array(6)).map((_, i) =>
        songFactory({ position: i + 1 })
      )
    }),
    categories: [],
    type: 'playlist',
     
    loadArtists: () => Promise.resolve([]),
     
    loadAuthors: () => Promise.resolve([]),
     
    loadSongs: () => Promise.resolve([])
  }
};
