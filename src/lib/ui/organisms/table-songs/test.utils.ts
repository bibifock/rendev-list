import SongFactory from '$lib/utils/testing/factory/Song';
import faker from '$lib/utils/testing/faker';
import type { Song } from '$lib/types';

const link = 'https://www.youtube.com/watch?v=-G3MLjqicC8';

const baseSongs: Partial<Song>[] = [
  { href: faker.internet.url() },
  { link },
  { link, fullpath: faker.internet.url() },
  { fullpath: faker.internet.url() },
  { fullpath: faker.internet.url(), isA3: 1 }
  // TODO Reactivate later
  // { unsaved: 1 },
  // { href: faker.internet.url(), unsaved: 1 }
];

const songs: Song[] = baseSongs.map((v, index) =>
  SongFactory({ ...v, id: index + 1, position: index === 0 ? 1 : undefined })
);

export const defaultProps = {
  sort: {
    value: 'author',
    asc: true
  },
  songs
};
