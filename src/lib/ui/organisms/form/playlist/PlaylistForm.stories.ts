import { defaultProps as songsProps } from '$lib/ui/organisms/table-songs/test.utils';
import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';

import Playlist from './PlaylistForm.svelte';


const meta: CustomMeta<typeof Playlist> = {
  title: 'organisms/form/playlistForm',
  component: Playlist,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
  tags: ['autodocs'],
  args: {
    name: '2019-12-01',
    image: {
      alt: 'image',
      src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRWbZwm35QoiGxyF6ZXup1ea7kDzwd59MF-EEaGI7WZqjFtbwoQ&s'
    },
    urlGenerate: '/url-to-help',
    playlist:
      'https://www.youtube.com/playlist?list=PLibS5rbMkb5RJ-N2DBHgXVUWSXULkJezn',
    file: {
      fullpath: 'upload/something.pdf',
      name: '2019-12-01.pdf'
    },
    songs: [],
    categories: [],
    loadWishes: () => Promise.resolve([]),
    loadArtists: () => Promise.resolve([]),
    loadAuthors: () => Promise.resolve([])
  },
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/svelte/configure/story-layout
    layout: 'fullscreen'
  }
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

export const Default: Story = {};

export const Errors: Story = {
  args: {
    errors: ['name', 'image', 'playlist'].reduce<Record<string, string>>(
      (o, key) => ({ ...o, [key]: 'this is an error' }),
      {
        system:
          'Am I going crazy? Have my years of wild hedonism finally caught up with me?'
      }
    )
  }
};

export const WithSongs: Story = {
  args: songsProps
};
