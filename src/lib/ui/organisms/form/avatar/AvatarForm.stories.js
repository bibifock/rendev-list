import { generateActions } from '$lib/utils/testing/storybook';

import Component from './AvatarForm';

const getStory = (props = {}) => ({
  Component,
  props,
  on: generateActions(['change'])
});

export default {
  title: 'organisms/form/avatar'
};

export const Base = () => getStory();

export const Image = () =>
  getStory({
    src: 'https://www.fillmurray.com/200/300'
  });

export const FileUploaded = () =>
  getStory({
    src: 'https://www.fillmurray.com/200/300',
    file: {
      name: 'fill-murray.png',
      path: '/tmp/deldkle'
    }
  });
