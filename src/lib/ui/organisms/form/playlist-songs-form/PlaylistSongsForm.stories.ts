import { defaultProps as songsProps } from '$lib/ui/organisms/table-songs/test.utils';
import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';

import component from './PlaylistSongsForm.svelte';


const meta: CustomMeta<typeof component> = {
  title: 'organisms/form/playlist-songs-form',
  component,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
  tags: ['autodocs'],
  args: {
    songs: []
  },
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/svelte/configure/story-layout
    layout: 'fullscreen'
  }
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

export const Default: Story = {};

export const WithSongs: Story = {
  args: {
    songs: [
      ...songsProps.songs.map((song, index) => ({
        ...song,
        id: index + 1,
        href: 'https://perdu.com'
      })),
      ...songsProps.songs.map((song, index) => ({
        ...song,
        href: 'https://perdu.com',
        id: 990 * (index + 1)
      }))
    ]
  }
};
