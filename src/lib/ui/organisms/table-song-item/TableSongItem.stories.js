import SongFactory from '$lib/utils/testing/factory/Song';
import { generateActions } from '$lib/utils/testing/storybook';

import Component from './TableSongItem.svelte';

const on = generateActions(['delete', 'positionChange']);

const getStory =
  (props = {}) =>
  () => ({
    Component,
    props: SongFactory({
      fullpath: 'uploads/tab.pdf',
      link: 'https://www.youtube.com/watch?v=5UWOiDJNAuA',
      href: 'edit/tab/1',
      ...props
    }),
    on
  });

export default {
  title: 'organisms/table-song-item'
};

export const Filled = getStory();

export const A3 = getStory({ isA3: true });

export const Edition = getStory({ edition: true });

export const WithoutLink = getStory({
  fullpath: null,
  link: null
});

export const Unsaved = getStory({
  unsaved: true
});
