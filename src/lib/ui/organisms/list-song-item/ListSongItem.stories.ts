import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';

import component from './ListSongItem.svelte';
import { defaultProps } from './test.utils';


const meta: CustomMeta<typeof component> = {
  title: 'organisms/list-song-item',
  component,
  args: defaultProps
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

export const Default: Story = {};

export const Compact = { args: { compact: true } };

export const NoLink = { args: { link: '' } };
export const BadLink = { args: { link: 'http://perdu.com' } };
