export type NavLinkType = {
  href: string;
  label: string;
  icon?: string;
  id: string;
};
