import PlaylistFactory from '$lib/utils/testing/factory/Playlist';
import SongFactory from '$lib/utils/testing/factory/Song';
import faker from '$lib/utils/testing/faker';

const songs = [];
for (let i = 0; i < 25; i++) {
  songs.push(
    SongFactory({
      position: i + 1,
      href: faker.internet.url(),
      fullpath: faker.internet.url(),
      isA3: i % 5 === 0
    })
  );
}

export const playlist = PlaylistFactory({
  songs,
  href: faker.internet.url(),
  file: {
    fullpath: faker.internet.url(),
    name: faker.system.fileName()
  }
});
