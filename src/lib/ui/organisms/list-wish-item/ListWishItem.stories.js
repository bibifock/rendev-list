import Component from './ListWishItem.svelte';
import { defaultProps } from './test.utils';

export default {
  title: 'organisms/list-wish-item'
};

const getStory = (props = {}) => ({
  Component,
  props: {
    ...defaultProps,
    ...props
  }
});

export const Base = () => getStory();

export const MissingFile = () => getStory({ href: 'https://pinute.org' });

export const Open = () => getStory({ open: true });

export const Compact = () => getStory({ compact: true });
