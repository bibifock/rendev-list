import { defaultProps as file } from '$lib/ui/molecules/file/test.utils';
import { playlists } from '$lib/ui/organisms/list-wish-item/test.utils';

export const defaultProps = {
  href: '/songs/id',
  title: 'A la belle étoile',
  artist: "Les yeux d'la tête",
  author: 'bbr',
  link: 'https://www.youtube.com/embed/esfJwdSl_7o',
  category: 'Chanson Française',
  level: 2,
  fileId: 1,
  created: new Date(),
  updated: new Date(),
  fullpath: file.fullpath,
  file,
  files: [
    { ...file, label: 'CM' },
    { ...file, label: 'A3' }
  ],
  playlists
};
