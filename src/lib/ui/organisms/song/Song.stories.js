import { generateActions } from '$lib/utils/testing/storybook';

import Component from './Song.svelte';
import { defaultProps as props } from './test.utils';

export default {
  title: 'organisms/song'
};

export const Base = () => ({
  Component,
  props,
  on: generateActions(['click', 'cancel'])
});

export const Detail = () => ({
  Component,
  props: {
    ...props,
    open: true
  }
});

export const NoLink = () => ({
  Component,
  props: {
    ...props,
    link: ''
  }
});

export const BadLink = () => ({
  Component,
  props: {
    ...props,
    link: 'http://perdu.com'
  }
});
