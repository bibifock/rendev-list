import Component from './SectionTitleStories.svelte';

const storyProps = {
  Component,
  props: {}
};

export default {
  title: 'atoms/section-title'
};

export const Base = () => storyProps;
