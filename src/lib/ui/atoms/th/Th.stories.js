import Component from './Th.svelte';

export default {
  title: 'atoms/th'
};

const label = 'label';

export const Base = () => ({
  Component,
  props: {
    label
  }
});

export const SortedAsc = () => ({
  Component,
  props: {
    sorted: true,
    asc: true
  }
});

export const SortedDesc = () => ({
  Component,
  props: {
    sorted: true,
    asc: false
  }
});
