import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';

import caption from './Caption.svelte';


const meta: CustomMeta<typeof caption> = {
  title: 'atoms/Caption',
  component: caption,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
  tags: ['autodocs'],
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/svelte/configure/story-layout
    layout: 'fullscreen'
  }
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

export const Default: Story = {};
