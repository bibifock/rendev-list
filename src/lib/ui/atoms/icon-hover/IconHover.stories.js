import Component from './IconHover';

const storyProps = {
  Component,
  on: {
    click: () => alert('click')
  }
};

export default {
  title: 'atoms/icon-hover'
};

export const Base = () => storyProps;

export const Selected = () => ({
  ...storyProps,
  props: {
    selected: true
  }
});
