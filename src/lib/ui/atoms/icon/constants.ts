import FaFacebookSquare from 'svelte-icons/fa/FaFacebookSquare.svelte';
import FaGitlab from 'svelte-icons/fa/FaGitlab.svelte';
import FaGripVertical from 'svelte-icons/fa/FaGripVertical.svelte';
import FaInstagram from 'svelte-icons/fa/FaInstagram.svelte';
import FaSoundcloud from 'svelte-icons/fa/FaSoundcloud.svelte';
import IoIosAttach from 'svelte-icons/io/IoIosAttach.svelte';
import IoIosBug from 'svelte-icons/io/IoIosBug.svelte';
import IoIosHelp from 'svelte-icons/io/IoIosHelp.svelte';
import IoIosMusicalNote from 'svelte-icons/io/IoIosMusicalNote.svelte';
import IoIosMusicalNotes from 'svelte-icons/io/IoIosMusicalNotes.svelte';
import IoLogoYoutube from 'svelte-icons/io/IoLogoYoutube.svelte';
import IoMdAdd from 'svelte-icons/io/IoMdAdd.svelte';
import IoMdCreate from 'svelte-icons/io/IoMdCreate.svelte';
import IoMdEye from 'svelte-icons/io/IoMdEye.svelte';
import IoMdEyeOff from 'svelte-icons/io/IoMdEyeOff.svelte';
import IoMdImages from 'svelte-icons/io/IoMdImages.svelte';
import IoMdLink from 'svelte-icons/io/IoMdLink.svelte';
import IoMdMenu from 'svelte-icons/io/IoMdMenu.svelte';
import IoMdStar from 'svelte-icons/io/IoMdStar.svelte';
import IoMdStarOutline from 'svelte-icons/io/IoMdStarOutline.svelte';
import IoMdTrash from 'svelte-icons/io/IoMdTrash.svelte';
import IoMdWarning from 'svelte-icons/io/IoMdWarning.svelte';
import MdArrowDropDown from 'svelte-icons/md/MdArrowDropDown.svelte';
import MdArrowDropUp from 'svelte-icons/md/MdArrowDropUp.svelte';

export const icons = {
  add: IoMdAdd,
  arrowDown: MdArrowDropDown,
  arrowUp: MdArrowDropUp,
  attach: IoIosAttach,
  bug: IoIosBug,
  create: IoMdCreate,
  eye: IoMdEye,
  eyeOff: IoMdEyeOff,
  facebook: FaFacebookSquare,
  gitlab: FaGitlab,
  grip: FaGripVertical,
  help: IoIosHelp,
  images: IoMdImages,
  instagram: FaInstagram,
  link: IoMdLink,
  menu: IoMdMenu,
  musicalNote: IoIosMusicalNote,
  musicalNotes: IoIosMusicalNotes,
  soundcloud: FaSoundcloud,
  star: IoMdStar,
  starOutline: IoMdStarOutline,
  trash: IoMdTrash,
  warning: IoMdWarning,
  youtube: IoLogoYoutube
};

export const isIconName = (icon: string): icon is keyof typeof icons =>
  icon in icons;
