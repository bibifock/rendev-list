import Component from './LastUpdate.svelte';

export default {
  title: 'atoms/last-update'
};

export const Base = () => ({
  Component,
  props: {
    value: new Date(2019, 10, 1)
  }
});

export const String = () => ({
  Component,
  props: {
    value: '2019-12-13 17:41:45'
  }
});
