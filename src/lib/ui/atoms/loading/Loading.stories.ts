import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';

import Component from './Loading.story.svelte';


const meta: CustomMeta<typeof Component> = {
  title: 'atoms/loading',
  component: Component,
  args: {
    onLoadMore: () => alert('load more')
  }
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

export const Base: Story = {};

export const Loading: Story = {
  args: {
    isLoading: true
  }
};
