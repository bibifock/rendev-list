import Component from './BaseField.svelte';

export default {
  title: 'atoms/fields/base'
};

export const Base = () => ({
  Component,
  props: { label: 'label' }
});

export const Error = () => ({
  Component,
  props: { label: 'label', error: 'my-error' }
});
