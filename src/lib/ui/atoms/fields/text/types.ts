export type CustomInputEvent = Event & {
  currentTarget: EventTarget & HTMLInputElement;
};
