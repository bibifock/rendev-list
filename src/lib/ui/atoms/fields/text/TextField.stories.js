import { generateActions } from '$lib/utils/testing/storybook';

import Component from './TextField.svelte';

const on = generateActions(['change']);

export default {
  title: 'atoms/fields/text'
};

export const Base = () => ({
  Component,
  props: {
    label: 'label',
    value: 'value'
  },
  on
});

export const Small = () => ({
  Component,
  props: {
    label: 'label',
    value: 'value',
    small: true
  },
  on
});

export const Number = () => ({
  Component,
  props: {
    label: 'label',
    value: 10,
    type: 'number'
  },
  on
});

export const Error = () => ({
  Component,
  props: {
    label: 'label',
    value: 'value',
    error: 'error message'
  },
  on
});
