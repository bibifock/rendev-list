import { generateActions } from '$lib/utils/testing/storybook';

import Component from './CancelButton.svelte';

const storyProps = {
  Component,
  props: {},
  on: generateActions(['cancel'])
};

export default {
  title: 'atoms/buttons/cancel'
};

export const Base = () => storyProps;
