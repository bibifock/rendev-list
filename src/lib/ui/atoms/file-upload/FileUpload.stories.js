import { generateActions } from '$lib/utils/testing/storybook';

import Component from './FileUpload.svelte';

export default {
  title: 'atoms/file-upload'
};

const on = generateActions(['change']);
const baseProps = { Component, on };

export const Base = () => baseProps;

export const WithLabel = () => ({
  ...baseProps,
  props: {
    accept: 'application/pdf',
    label: 'add a file'
  }
});
