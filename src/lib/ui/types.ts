import type {
  ComponentProps,
  SvelteComponent,
  Component as ComponentSvelte
} from 'svelte';

export type Option<Value = number | string | undefined> = {
  value: Value;
  label: string;
};

export type FnAutoComplete<T = number | string | undefined> = (
  search: string
) => Promise<Option<T>[]>;

export type Sort = { value: string; asc: boolean };

export type Dispatcher<
  TEvents extends Record<keyof TEvents, CustomEvent<unknown>>
> = {
  [Property in keyof TEvents]: TEvents[Property]['detail'];
};

export type EventTarget<T = Element, E = Event> = E & {
  currentTarget: EventTarget & T;
};

export interface SongItem {
  href?: string;
  position?: number;
  title: string;
  artist: string;
  author?: string;
  level?: number;
  edition?: boolean;
  fullpath?: string;
  link?: string;
  isA3?: number;
  unsaved?: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type DefaultComponentSvelte = SvelteComponent | ComponentSvelte<any, any>;

export type ComponentEventArgs<
  Component extends DefaultComponentSvelte,
  EventName extends keyof ComponentProps<Component>
> = Parameters<ComponentProps<Component>[EventName]>[0];
