import getStory from '$lib/utils/storybook/getStory';
import faker from '$lib/utils/testing/faker';

import Component from './Title';

const story = getStory({
  Component,
  title: 'molecules/Title',
  argTypes: {
    title: { control: 'text' },
    subtitle: { control: 'text' },
    href: { control: 'text' },
    target: { control: 'text', defaultValue: '_blank' },
    youtube: { control: 'text' },
    file: { control: 'text' },
    isA3: { control: 'boolean' }
  },
  args: {
    title: 'I fought the law (bobby Fuller)',
    subtitle: 'Bobby Fuller Tour'
  },
  parameters: {
    layout: 'padded'
  }
});

export default story.default;

export const Base = story.bind();

export const Link = story.bindWithArgs({
  href: faker.internet.url(),
  target: '_blank'
});

export const Filled = story.bindWithArgs({
  href: faker.internet.url(),
  target: '_blank',
  youtube: 'https://www.youtube.com/watch?v=5UWOiDJNAuA',
  file: faker.internet.url(),
  isA3: true
});
