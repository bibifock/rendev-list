import Component from './Avatar.svelte';

export default {
  title: 'molecules/avatar'
};

export const Base = () => ({ Component });

export const WithImage = (props = {}) => ({
  Component,
  props: {
    src: 'https://www.fillmurray.com/200/300',
    alt: 'alternative text',
    ...props
  }
});

export const Small = () => WithImage({ small: true });
