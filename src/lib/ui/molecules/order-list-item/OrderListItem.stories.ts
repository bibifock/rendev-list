import faker from '$lib/utils/testing/faker';
import type { Option } from '$lib/ui/types';
import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';

import component from './OrderListItem.svelte';

const meta: CustomMeta<typeof component> = {
  title: 'molecules/order-list-item',
  component,
  args: {
    position: 1,
    id: 1,
    title: faker.lorem.sentence(),
    subtitle: faker.name.findName(),
    href: faker.internet.url(),
    file: faker.internet.url(),
    youtube: 'https://www.youtube.com/watch?v=KzM5yoo8Y08',
    positionOptions: [1, 2, 3].map(
      (value: number): Option<number> => ({
        value,
        label: `${value}`
      })
    )
  }
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

export const Default: Story = {};

export const A3: Story = { args: { isA3: true } };
