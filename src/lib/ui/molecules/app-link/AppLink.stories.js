import Component from './AppLinkStories';

const getStory =
  (props = {}) =>
  () => ({
    Component,
    props: {
      links: [
        'https://www.youtube.com/channel/UC_kUcxY8_vdxBFNAK-8vDUQ',
        'https://www.facebook.com/groups/rendevuke',
        'https://soundcloud.com/rendevuke-985400253',
        'https://gitlab.com/bibifock/rendev-list',
        'http://perdu.com'
      ],
      ...props
    }
  });

export default {
  title: 'molecules/app-link'
};

export const Base = getStory();
export const Medium = getStory({ medium: true });
export const White = getStory({ white: true, medium: true });
