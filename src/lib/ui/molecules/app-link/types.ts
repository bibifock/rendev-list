export interface AppLinkComponentProps {
  onMouseover?: () => void;
  onMouseout?: () => void;
  href: string;
  color?: string | undefined;
  medium?: boolean;
  large?: boolean;
}
