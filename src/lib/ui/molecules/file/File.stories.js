import { generateActions } from '$lib/utils/testing/storybook';

import Component from './File.svelte';
import { defaultProps } from './test.utils';

export default {
  title: 'molecules/file'
};

const getStory = (props = {}) => ({
  Component,
  props: {
    ...defaultProps,
    ...props
  },
  on: generateActions(['select', 'change'])
});

export const Base = () => getStory();

export const UnuploadedFile = () =>
  getStory({
    type: 'pdf',
    name: "I'me waiting for a validation",
    path: '/tmp/balbla.pdf',
    fullpath: '',
    updated: null
  });

export const Selected = () =>
  getStory({
    selected: true
  });

export const Displayed = () =>
  getStory({
    display: true,
    label: 'A3'
  });

export const FormatA3 = () =>
  getStory({
    isA3: true
  });
