import Component from './YoutubePreview.svelte';

export default {
  title: 'atoms/youtube-preview'
};

export const Base = () => ({
  Component,
  props: { link: 'https://www.youtube.com/embed/esfJwdSl_7o' }
});

export const List = () => ({
  Component,
  props: {
    link: 'https://www.youtube.com/playlist?list=PLibS5rbMkb5QHUnOUA_12lidK5Xx6-tyJ'
  }
});

export const NoPreview = () => ({
  Component,
  props: { link: 'http://www.perdu.com/' }
});
