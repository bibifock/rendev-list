import faker from '$lib/utils/testing/faker';
import type { CustomMeta, CustomStoryObj } from '$lib/utils/storybook/types';

import Component from './SongLinksStories.svelte';


const meta: CustomMeta<typeof Component> = {
  title: 'molecules/song-links',
  component: Component,
  args: {
    youtube: 'https://www.youtube.com/watch?v=-G3MLjqicC8',
    file: faker.internet.url(),
    isA3: 1
  }
};

export default meta;

type Story = CustomStoryObj<typeof meta>;

export const Base: Story = {};

export const Medium: Story = { args: { medium: true } };
