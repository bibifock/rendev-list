export const preventDefault = <T extends () => void>(fn?: T) =>
  fn
    ? (e: Event) => {
        e.preventDefault();
        fn();
      }
    : undefined;

export const stopPropagation = <T extends () => void>(fn?: T) =>
  fn
    ? (e: Event) => {
        e.preventDefault();
        fn();
      }
    : undefined;
