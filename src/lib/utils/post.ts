const post = <Response, Params = Record<string, string>>(
  endpoint: string,
  data: Params
): Promise<Response> => {
  return fetch(endpoint, {
    method: 'POST',
    credentials: 'include',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(async (r) => {
    const result = await r.json();
    if (r.ok) {
      return result as Response;
    }

    throw result;
  });
};

export default post;
