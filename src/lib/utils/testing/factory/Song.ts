import { faker } from '$lib/utils/testing/faker';
import type { Song } from '$lib/types';

const SongFactory = (opt: Partial<Song> = {}): Song => ({
  title: faker.lorem.sentence(),
  author: faker.name.firstName(),
  artist: faker.name.findName(),
  href: faker.internet.url(),
  link: 'https://www.youtube.com/watch?v=5UWOiDJNAuA',
  categoryId: 1,
  category: 'Chanson Française',
  level: Math.floor(Math.random() * 10),
  created: faker.date.past(),
  updated: faker.date.recent(),
  ...opt
});

export default SongFactory;
