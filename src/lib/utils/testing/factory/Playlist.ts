import { faker } from '$lib/utils/testing/faker';
import type { Playlist } from '$lib/types';

export const playlistFactory = (opt = {}): Partial<Playlist> => ({
  name: faker.date.past().toISOString().replace(/T.+/, ''),
  playlist:
    'https://www.youtube.com/playlist?list=PLibS5rbMkb5QHUnOUA_12lidK5Xx6-tyJ',
  image: {
    src: 'https://www.fillmurray.com/200/300',
    alt: 'alt'
  },
  file: faker.system.filePath(),
  updated: faker.date.recent(),
  songs: [],
  type: 'playlist',
  ...opt
});
