export const getFormDataString = (
  data: FormData,
  key: string
): string | undefined => {
  const entry = data.get(key);
  if (typeof entry === 'string') {
    return entry;
  }
  return undefined;
};
