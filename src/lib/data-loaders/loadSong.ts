import { error } from '@sveltejs/kit';

import playlistModel from '$lib/models/playlist/Playlist';
import songModel from '$lib/models/song/Song';
import prepareSong from '$lib/views/song/list/prepareSong';
import type { Song } from '$lib/types';

export const loadSong = async ({
  id,
  logged
}: {
  id: number;
  logged?: boolean;
}): Promise<Song> => {
  const needsSongs = logged;

  const song = await songModel.getById(id, needsSongs);

  if (!song) {
    error(404, 'Song not found');
  }

  const playlists = await playlistModel.getBySongId(id, needsSongs);

  return prepareSong({
    ...song,
    playlists
  });
};
