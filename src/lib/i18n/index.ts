import { register, init, waitLocale } from 'svelte-i18n';

// eslint-disable-next-line vitest/require-hook
register('fr', () => import('./locales/fr.json'));
// eslint-disable-next-line vitest/require-hook
register('en', () => import('./locales/en.json'));

export const loadLocale = async () => {
  init({ initialLocale: 'fr', fallbackLocale: 'fr' });
  await waitLocale();
};
