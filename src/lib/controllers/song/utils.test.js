import { describe, it, beforeEach, expect } from 'vitest';

import { haveError, getFilesToAdd } from './haveError';

describe('app/controllers/songs utils', () => {
  describe('getFilesToAdd', () => {
    describe('should return empty array with', () => {
      it.each([undefined, [], [{}]])('%p', (files) => {
        expect(getFilesToAdd(files)).toStrictEqual(expect.any(Array));
      });
    });

    it('should only return all files', () => {
      const files = [
        { test: true },
        { src: true },
        { src: false },
        { test: 'jkejkde', src: 'dede' }
      ];

      expect(getFilesToAdd(files)).toHaveLength(files.length);
    });
  });

  describe('haveError', () => {
    let props;
    // eslint-disable-next-line vitest/no-hooks
    beforeEach(() => {
      props = {
        title: 'A la belle étoile',
        artist: "Les yeux d'la tête",
        author: 'bbr',
        link: 'https://www.youtube.com/watch?v=esfJwdSl_7o',
        categoryId: 1,
        level: 2
      };
    });

    it('should be valid', () => {
      expect(haveError(props)).toBeFalsy();
    });

    it('should return succes value', () => {
      const toto = 'toto';
      expect(haveError(props, toto)).toBe(toto);
    });

    it('should be false with empty values', () => {
      const fields = ['artist', 'categoryId', 'title'];
      const wanted = fields.reduce((obj, key) => {
        props[key] = null;
        return { ...obj, [key]: expect.any(String) };
      }, {});
      expect(haveError(props)).toStrictEqual(wanted);
    });

    it('should detect bad links', () => {
      props.link = 'prepod';
      expect(haveError(props)).toStrictEqual({ link: expect.any(String) });
    });
  });
});
