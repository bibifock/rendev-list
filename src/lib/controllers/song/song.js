import File from '$lib/models/file/File';
import Playlist from '$lib/models/playlist/Playlist';
import Song from '$lib/models/song/Song';

import { haveError, getFilesToAdd } from './haveError';

const needsSongs = ({ session }) => session && session.user;

const getFullpath = ({ fullpath }) => fullpath;

const insertFilesForSong = async ({ songId, files }) => {
  const filesToUpdate = getFilesToAdd(files, songId);

  if (!filesToUpdate.length) {
    return true;
  }

  const filesToAdd = filesToUpdate.filter(({ id }) => !id);

  // check news files aren't already in dasongase
  const fullpaths = filesToAdd.map(getFullpath);
  const results = await File.alreadyExists({ songId, fullpaths });

  if (results) {
    return results.map(getFullpath);
  }

  const fileId = await File.saveAll(filesToUpdate);
  if (fileId === true) {
    return true;
  }

  // ici on a récupérée la nouvelle partition sélectionnée
  await Song.selectFile({ songId, fileId });

  return true;
};

export const getById = async ({ params: { id }, session }) => {
  const [song, playlists] = await Promise.all([
    Song.getById(id, needsSongs({ session })),
    Playlist.getBySongId(id, needsSongs({ session }))
  ]);

  return {
    ...song,
    playlists
  };
};

export const save = async ({ params: { id } = {}, body, session }) => {
  id = parseInt(id);
  const { files, ...song } = body;

  const errors = haveError(song, {});

  const result = await Song.alreadyExists(song);
  if (result && id !== result.id) {
    const { id: rowid, title } = result;
    errors.title = `${title} already present <a href="/song/${rowid}" class="alert-link" target="_blank">here</a>`;
  }

  if (Object.keys(errors).length) {
    throw errors;
  }

  const { id: songId } = await Song.save(song);

  await insertFilesForSong({ songId, files });

  return {
    success: true,
    id: songId,
    song: await Song.getById(songId, needsSongs({ session }))
  };
};

const orderByParam = {
  titleAsc: 'songs.title ASC, songs.artist ASC',
  titleDesc: 'songs.title DESC, songs.artist ASC',
  artistAsc: 'songs.artist asc, songs.title asc',
  artistDesc: 'songs.artist desc, songs.title asc',
  createdDesc: 'songs.created desc, songs.title ASC, songs.artist ASC',
  updatedDesc: 'songs.updated desc, songs.title ASC, songs.artist ASC'
};

export const getAll = async ({
  search,
  sort,
  limit,
  page,
  withFiles,
  categoryId,
  level
}) => {
  const orderBy = orderByParam[sort];

  const result = await Song.all({
    search,
    orderBy,
    limit,
    page,
    withFiles,
    categoryId,
    level
  });

  return result;
};

export const getWishes = ({ search, sort }) => {
  return Song.getWishes({
    search,
    orderBy: orderByParam[sort]
  });
};

export default {
  getById,
  getAll,
  save
};
