declare module 'svelte-icons/fa/*.svelte' {
  const e: unknown;
  export default e;
}

declare module 'svelte-icons/io/*.svelte' {
  const e: SvelteComponentTyped;
  export default e;
}

declare module 'svelte-icons/md/*.svelte' {
  const e: unknown;
  export default e;
}
